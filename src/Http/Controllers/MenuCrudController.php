<?php

namespace SoluAdmin\MenusCrud\Http\Controllers;

use SoluAdmin\MenusCrud\Http\Requests\MenuRequest as StoreRequest;
use SoluAdmin\MenusCrud\Http\Requests\MenuRequest as UpdateRequest;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class MenuCrudController extends BaseCrudController
{

    public function setUp()
    {
        parent::setup();
        $this->crud->addButtonFromModelFunction('line', 'see_items', 'showItemsButton', 'beginning');
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud($request);
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud($request);
    }
}
