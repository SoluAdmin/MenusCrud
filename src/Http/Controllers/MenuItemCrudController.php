<?php

namespace SoluAdmin\MenusCrud\Http\Controllers;

use SoluAdmin\MenusCrud\Http\Requests\MenuItemRequest as StoreRequest;
use SoluAdmin\MenusCrud\Http\Requests\MenuItemRequest as UpdateRequest;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class MenuItemCrudController extends BaseCrudController
{
    public $menuId;
    public $itemId;

    public function __construct()
    {
        parent::__construct();

        // Need to check for \Route::current() to avoid breaking route:list artisan command
        $this->menuId = \Route::current()
            ? \Route::current()->parameter('menu')
            : null;

        $this->itemId = \Route::current()
            ? \Route::current()->parameter('item')
            : null;
    }

    public function route()
    {
        return "menu/{$this->menuId}/item";
    }

    public function setUp()
    {
        parent::setup();
        $this->crud->addClause('where', 'menu_id', $this->menuId);
        $this->crud->enableReorder('name', 0);
        $this->crud->allowAccess('reorder');
    }

    public function edit($id)
    {
        return parent::edit($this->itemId);
    }

    public function store(StoreRequest $request)
    {
        $request->request->add(['menu_id' => $this->menuId]);
        return parent::storeCrud($request);
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud($request);
    }

    public function destroy($id)
    {
        return parent::destroy($this->itemId);
    }
}
