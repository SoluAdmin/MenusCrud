<?php

namespace SoluAdmin\MenusCrud\Http\DataTables;

use SoluAdmin\MenusCrud\Models\MenuItem;
use SoluAdmin\Support\Interfaces\DataTable;

class MenuItemCrudDataTable implements DataTable
{
    public function columns()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::MenusCrud.name')
            ],
            [
                'label' => trans('SoluAdmin::MenusCrud.parent'),
                'type' => 'select',
                'name' => 'parent_id',
                'entity' => 'parent',
                'attribute' => 'name',
                'model' => MenuItem::class,
            ],
            [
                'type' => 'model_function',
                'function_name' => 'url',
                'label' => trans('SoluAdmin::MenusCrud.link')
            ]
        ];
    }
}
