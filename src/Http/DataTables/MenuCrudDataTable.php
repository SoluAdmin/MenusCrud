<?php

namespace SoluAdmin\MenusCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class MenuCrudDataTable implements DataTable
{
    public function columns()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::MenusCrud.name'),
            ],
        ];
    }
}
