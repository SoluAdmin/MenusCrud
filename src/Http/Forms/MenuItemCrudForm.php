<?php

namespace SoluAdmin\MenusCrud\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class MenuItemCrudForm implements Form
{

    public function fields()
    {
        return [
            [
                'label' => trans('SoluAdmin::MenusCrud.name'),
                'name' => 'name',
                'type' => 'text'
            ],
            [
                'label' => trans('SoluAdmin::MenusCrud.item_type'),
                'name' => 'type',
                'type' => 'page_or_link',
                'page_model' => config('SoluAdmin.MenusCrud.page_model'),
            ],
        ];
    }

    public function advancedFields()
    {
        return [
            [
                'name' => 'separator',
                'type' => 'custom_html',
                'value' => '<hr><h4>' . trans('SoluAdmin::MenusCrud.advanced') . '</h4><br>' .
                    '<p>' . trans('SoluAdmin::MenusCrud.advanced_warning') . '</p>',
            ],
            [
                'name' => 'routes',
                'label' => trans('SoluAdmin::MenusCrud.routes'),
                'type' => 'table',
                'entity_singular' => trans('SoluAdmin::MenusCrud.route'),
                'columns' => [
                    'name' => trans('SoluAdmin::MenusCrud.route'),
                ],
            ],
        ];
    }
}
