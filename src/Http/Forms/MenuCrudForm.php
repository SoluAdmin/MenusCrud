<?php

namespace SoluAdmin\MenusCrud\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class MenuCrudForm implements Form
{

    public function fields()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::MenusCrud.name'),
            ],
        ];
    }
}
