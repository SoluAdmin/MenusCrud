<?php

namespace SoluAdmin\MenusCrud\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MenuRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
        ];
    }
}
