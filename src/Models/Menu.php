<?php

namespace SoluAdmin\MenusCrud\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use CrudTrait;

    protected $fillable = ['name'];

    public function items()
    {
        return $this->hasMany(MenuItem::class)->orderBy('lft');
    }

    public function getTree()
    {
        return $this->getSubtree($this->items, null);
    }

    public function getSubtree($items, $parentId)
    {
        $children = $items->where('parent_id', $parentId);

        if ($children->count() === 0) {
            return false;
        }

        return $children->map(function ($child) use ($items) {
            $child->subTree = $this->getSubtree($items, $child->id);
            return $child;
        });
    }

    public function showItemsButton()
    {
        $itemUrl = \Request::url() . '/' . $this->id . "/item";
        $label = trans('SoluAdmin::MenusCrud.see_items');

        return <<< EOD
        <a href="$itemUrl" class="btn btn-xs btn-default">
            <i class="fa fa-eye"></i>
            $label
        </a>
EOD;
    }
}
