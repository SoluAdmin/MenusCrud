<?php

namespace SoluAdmin\MenusCrud\Models;

use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $with = ['page'];
    protected $fillable = ['name', 'type', 'link', 'menu_id', 'parent_id'];
    protected $translatable = ['name', 'link'];
    protected $casts = [
        'routes' => 'array'
    ];

    public function menu()
    {
        $this->belongsTo(Menu::class);
    }

    public function parent()
    {
        return $this->belongsTo(MenuItem::class);
    }

    public function children()
    {
        return $this->hasMany(MenuItem::class, 'parent_id');
    }
    public function page()
    {
        return $this->belongsTo(config('SoluAdmin.MenusCrud.page_model'), 'page_id');
    }

    public function url()
    {
        switch ($this->type) {
            case 'external_link':
                return $this->link;
                break;
            case 'internal_link':
                return is_null($this->link) ? '#' : url($this->link);
                break;
            default:
                return url($this->page->slug);
                break;
        }
    }

    public function getActivableRoutesAttribute()
    {
        return collect(json_decode($this->attributes['routes'], true))->map(function ($item) {
            return $item['name'];
        })->toArray();
    }
}
