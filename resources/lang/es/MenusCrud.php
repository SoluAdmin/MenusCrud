<?php

return [
    'menu_manager' => 'Gestor de menus',
    'menu_plural' => 'Menus',
    'menu_singular' => 'Menu',
    'item_singular' => 'Elemento',
    'item_plural' => 'Elementos',
    'name' => 'Nombre',
    'see_items' => 'Ver Elementos',
    'item_type' => 'Tipo',
    'link' => 'Enlace',
    'parent' => 'Padre',
    'advanced' => 'Avanzado',
    'advanced_warning' => 'Modificar con cuidado, podría romper la web si se utiliza mal',
    'route' => 'Ruta',
    'routes' => 'Rutas',
];
