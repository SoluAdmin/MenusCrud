<?php

return [
    'menu_manager' => 'Menu Manager',
    'menu_plural' => 'Menus',
    'menu_singular' => 'Menu',
    'item_singular' => 'Menu Item',
    'item_plural' => 'Menu Items',
    'name' => 'Name',
    'see_items' => 'See Menu Items',
    'item_type' => 'Type',
    'link' => 'Link',
    'parent' => 'Parent',
    'advanced' => 'Advanced',
    'advanced_warning' => 'Modify this with care, it could break the site if done wrong',
    'route' => 'Route',
    'routes' => 'Routes',
];
