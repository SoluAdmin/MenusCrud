@module("SoluAdmin\\MenusCrud")
<li>
    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.MenusCrud.route_prefix', '') . '/menu') }}">
        <i class="fa fa-list"></i> <span>{{trans('SoluAdmin::MenusCrud.menu_manager')}}</span>
    </a>
</li>
@endmodule