<?php

return [
    'route_prefix' => '/menu-manager',
    'middleware' => false,
    'setup_routes' => false,
    'publishes_migrations' => false,
    'page_model' => 'App\\Models\\Page',
];
